# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-02-16 17:56+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/lib/docker.js:24
msgid "Start docker"
msgstr ""

#: src/lib/docker.js:28
msgid "Check if running"
msgstr ""

#: src/lib/docker.js:33
msgid "Start"
msgstr ""

#: src/lib/docker.js:37
msgid "Start interactive"
msgstr ""

#: src/lib/docker.js:41
msgid "Restart"
msgstr ""

#: src/lib/docker.js:45 src/lib/docker.js:82
msgid "Stop"
msgstr ""

#: src/lib/docker.js:49
msgid "Pause"
msgstr ""

#: src/lib/docker.js:53
msgid "Unpause"
msgstr ""

#: src/lib/docker.js:57 src/lib/docker.js:86 src/lib/docker.js:92
msgid "Remove"
msgstr ""

#: src/lib/docker.js:61
msgid "Exec Bash"
msgstr ""

#: src/lib/docker.js:65
msgid "Attach Terminal"
msgstr ""

#: src/lib/docker.js:69 src/lib/docker.js:104 src/modules/imageMenuIcons.js:28
msgid "Inspect"
msgstr ""

#: src/lib/docker.js:73
msgid "View logs"
msgstr ""

#: src/lib/docker.js:78
msgid "Up"
msgstr ""

#: src/lib/docker.js:96 src/modules/imageMenuIcons.js:20
msgid "Run"
msgstr ""

#: src/lib/docker.js:100 src/modules/imageMenuIcons.js:24
msgid "Run interactive"
msgstr ""

#: src/modules/containerMenu.js:52 src/ui/dialogs/information.js:25
msgid "Information"
msgstr ""

#: src/modules/containerMenu.js:55
msgid "copied to clipboard"
msgstr ""

#: src/modules/containerMenu.js:84
msgid "Ports"
msgstr ""

#: src/modules/menu.js:24 src/ui/menu.js:26
msgid "Docker Menu"
msgstr ""

#: src/prefs.js:28
msgid "General"
msgstr ""

#: src/prefs.js:35
msgid "Logo"
msgstr ""

#: src/prefs.js:44
msgid "Menu"
msgstr ""

#: src/prefs.js:54
msgid "Menu Quick Settings"
msgstr ""

#: src/prefs.js:67
msgid "Menu Text/Image"
msgstr ""

#: src/prefs.js:74
msgid "Show/Hide"
msgstr ""

#: src/prefs.js:83
msgid "Size"
msgstr ""

#: src/prefs.js:93
msgid "Technical"
msgstr ""

#: src/ui/dialogs/information.js:34
msgid "Close"
msgstr ""

#: src/ui/dialogs/confirm.js:23
msgid "Cancel"
msgstr ""

#: src/ui/dialogs/confirm.js:31
msgid "Ok"
msgstr ""

#: src/ui/menu.js:81
msgid "Empty"
msgstr ""
